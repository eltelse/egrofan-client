import { Component, Input, OnInit, Output, EventEmitter, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatSort, Sort } from '@angular/material/sort';

@Component({
  selector: 'app-custom-table',
  templateUrl: './custom-table.component.html',
  styleUrls: ['./custom-table.component.scss']
})
export class CustomTableComponent implements OnInit {

  constructor() { }

  @Input() displayedColumns: string[] = [];
  @Input() dataSource: MatTableDataSource<any> | undefined;
  @Input() pageSizeOptions: number[] = [5, 10, 25, 50];
  @Input() pageSize: number = 5;

  @Output("openEditModal") openEditModal: EventEmitter<any> = new EventEmitter();
  @Output("deleteObject") deleteObject: EventEmitter<any> = new EventEmitter();


  // Paging / Sorting / Filtering
  @ViewChild('tblPaginator') paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;


  ngOnInit(): void {
    this.refreshDataSourceProperties();
  }

  ngAfterViewInit() {
    this.refreshDataSourceProperties();
  }

  ngOnChanges() {
    this.refreshDataSourceProperties();
  }

  refreshDataSourceProperties() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;

  }

  sortData(sort: Sort) {
    //TBD - ?? console.log('sort - ' + sort);
  }

  pageChanged(event: PageEvent) {
    // TBD - scrall to top when changing from 100 recors per page to 10 or 5  
  }

  openEdit(element) {
    this.openEditModal.emit(element);
  }

  deleteElement(element) {
    this.deleteObject.emit(element);
  }

}
