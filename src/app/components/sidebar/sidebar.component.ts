import { Component, OnInit } from '@angular/core';
import { AppConfigService } from 'app/app-config.service';

declare const $: any;
declare interface RouteInfo {
    path: string;
    title: string;
    icon: string;
    class: string;
}
export const ROUTES: RouteInfo[] = [

    { path: '/map', title: 'Home',  icon:'map', class: '' },
    
    { path: '/vehicle', title: 'Vehicles',  icon:'directions_car', class: '' },
	  { path: '/work-station', title: 'Work Stations',  icon:'computer', class: '' },
    { path: '/user-list', title: 'Users', icon: 'people', class: '' },
    { path: '/inspection', title: 'Inspections', icon: 'location_searching', class: '' },
    { path: '/notification', title: 'Notifications', icon: 'notification_important', class: '' },

    { path: '/AreaType', title: 'Area Type', icon: 'settings', class: '' },
    { path: '/ConclusionType', title: 'Conclusion Type', icon: 'settings', class: '' },
    { path: '/DataCorrectionType', title: 'Data Correction Type', icon: 'settings', class: '' },
    { path: '/InspectionWorkflowType', title: 'Workflow Type', icon: 'settings', class: '' },
    // { path: '/DeviceCommandSourceType', title: 'Command Source Type', icon: 'settings', class: '' },
    // { path: '/DeviceEventType', title: 'Device Event Type', icon: 'settings', class: '' },
    // { path: '/DeviceStatusType', title: 'Device Status Type', icon: 'settings', class: '' },
    // { path: '/DeviceType', title: 'Device Type', icon: 'settings', class: '' },
    // { path: '/InspectionDataType', title: 'Inspection Data Type', icon: 'settings', class: '' },
    // { path: '/InspectionHistorySourceType', title: 'History Source Type', icon: 'settings', class: '' },
    // { path: '/VehicleType', title: 'Vehicle Type', icon: 'settings', class: '' },
    // { path: '/WorkStationType', title: 'WorkStation Type', icon: 'settings', class: '' },
    // { path: '/NotificationType', title: 'Notification Type', icon: 'settings', class: '' },
    
];

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {
  menuItems: any[];

  main_title = "";

  constructor(private appConfigService: AppConfigService) { }

  ngOnInit() {
    this.menuItems = ROUTES.filter(menuItem => menuItem);
    this.main_title = this.appConfigService.sideBarTitle;
  }
  isMobileMenu() {
      if ($(window).width() > 991) {
          return false;
      }
      return true;
  };
}
