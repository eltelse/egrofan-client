import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { NavbarComponent } from './navbar/navbar.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { CustomTableComponent } from './custom-table/custom-table.component';

import { UtilsModule } from 'app/utils/utils.module';


@NgModule({
  imports: [
    CommonModule,
    RouterModule,
   
    UtilsModule, // Angulare Material + More
    
  ],
  declarations: [
    NavbarComponent,
    SidebarComponent,
    CustomTableComponent,
    

  ],
  exports: [
    NavbarComponent,
    SidebarComponent,
    CustomTableComponent,
    
  ]
})
export class ComponentsModule { }
