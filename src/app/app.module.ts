// Angular Globals
import { APP_INITIALIZER, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { AppRoutingModule } from './app.routing';

// Angular Bootstrap (css)
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

// Project Imports
import { SharedService } from './shared.service';
import { ComponentsModule } from './components/components.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { DriverTagComponent } from './driver-tag/driver-tag.component';
import { MapComponent } from './map/map.component';
import { AdminLayoutComponent } from './layouts/admin-layout/admin-layout.component';
import { CheckinComponent } from './checkin/checkin.component';
import { PrintDriverTagComponent } from './checkin/print-driver-tag/print-driver-tag.component';


// Import from library
// https://costlydeveloper.github.io/ngx-awesome-popup/#/getting-started
import {
  NgxAwesomePopupModule,
  DialogConfigModule,
  ConfirmBoxConfigModule,
  ToastNotificationConfigModule
} from '@costlydeveloper/ngx-awesome-popup';
import { WorkflowsModule } from './workflows/workflows.module';
import { ManagementModule } from './management/management.module';
import { UtilsModule } from './utils/utils.module';
import { AppConfigService } from './app-config.service';


@NgModule({
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    ComponentsModule,
    NgbModule,
    RouterModule,
    AppRoutingModule,
    WorkflowsModule,
    ManagementModule,
    
    UtilsModule, // Angulare Material + More

    NgxAwesomePopupModule.forRoot(), // Essential, mandatory main module.
    DialogConfigModule.forRoot(), // Needed for instantiating dynamic components.
    ConfirmBoxConfigModule.forRoot(), // Needed for instantiating confirm boxes.
    ToastNotificationConfigModule.forRoot() // Needed for instantiating toast notifications.
  ],
  declarations: [
    AppComponent,
    LoginComponent,
    AdminLayoutComponent,
    DriverTagComponent,
    MapComponent,
    CheckinComponent,
    PrintDriverTagComponent
  ],
  providers: [
    SharedService,
    // For AppConfigService see: https://stackoverflow.com/questions/43193049/app-settings-the-angular-way
    {
      provide: APP_INITIALIZER,
      multi: true,
      deps: [AppConfigService],
      useFactory: (appConfigService: AppConfigService) => {
        return () => {
          //Make sure to return a promise!
          return appConfigService.loadAppConfig();
        };
      }
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
