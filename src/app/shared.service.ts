import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SharedService {
  readonly APIUrl = "https://localhost:5001/api";
  readonly PhotoUrl = "https://localhost:5001/photos";

  constructor(private http: HttpClient) { }

  /* USER SERVICE*/
  getUsersList(): Observable<any[]> {
    return this.http.get<any>(this.APIUrl + '/User')
  }

  // This list is a sub details list to be used at tables that relate to users but you dont want password (for example), or you might need FullName (not exist in the orig query)
  getUserDisplayList(): Observable<any[]> {
    return this.http.get<any>(this.APIUrl + '/Lists/GetUsersList')
  }


  addUser(val: any) {
    return this.http.post<any>(this.APIUrl + '/User/', val);
  }

  validateUserCredentials(val: any) {
    return this.http.post<any>(this.APIUrl + '/User/ValidateUserCredentials', val);
  }

  getUserDisplayColumns() {
    return this.http.get<any>(this.APIUrl + '/User/GetUserDisplayColumns');
  }

  UpdatePassword(val: any) {
    return this.http.put(this.APIUrl + '/User/UpdatePassword', val);
  }

  updateUserDetails(val: any) {
    return this.http.put(this.APIUrl + '/User/', val);
  }

  deleteUser(val: any) {
    return this.http.delete(this.APIUrl + '/User/' + val);
  }

  /* Work Stasions SERVICE*/
  getWorkStationList(): Observable<any[]> {
    return this.http.get<any>(this.APIUrl + '/WorkStation')
  }

  getWorkStationDisplayColumns() {
    return this.http.get<any>(this.APIUrl + '/WorkStation/GetWorkStationsDisplayColumns');
  }


  deleteWorkStation(val: any) {
    return this.http.delete(this.APIUrl + '/WorkStation/' + val);
  }

  addWorkStation(val: any) {
    return this.http.post<any>(this.APIUrl + '/WorkStation/', val);
  }

  updateWorkStation(val: any) {
    return this.http.put(this.APIUrl + '/WorkStation/', val);
  }

  getWorkStationsTypes() {
    return this.http.get<any>(this.APIUrl + '/Lists/GetWorkStationsTypes');
  }
  /* VEHICLE SERVICE*/
  getVehicleList(): Observable<any[]> {
    return this.http.get<any>(this.APIUrl + '/Vehicle')
  }

  getVehicleDisplayColumns() {
    return this.http.get<any>(this.APIUrl + '/Vehicle/GetVehiclesDisplayColumns');
  }

  updateVehicle(val: any) {
    return this.http.put(this.APIUrl + '/Vehicle/', val);
  }

  deleteVehicle(val: any) {
    return this.http.delete(this.APIUrl + '/Vehicle/' + val);
  }

  addVehicle(val: any) {
    return this.http.post(this.APIUrl + '/Vehicle/', val);
  }

  getVehicleTypesList(): Observable<any[]> {
    return this.http.get<any>(this.APIUrl + '/Lists/GetVehicleTypes')
  }


  UploadPhoto(val: any) {
    return this.http.post(this.APIUrl + '/Vehicle/SaveFile', val);
  }

  /* Inspection type / workflow SERVICE*/
  getInspectionTypesList(): Observable<any[]> {
    return this.http.get<any>(this.APIUrl + '/Lists/GetInspectionTypes')
  }

  // Conclusion type
  getConclusionTypesList(): Observable<any[]> {
    return this.http.get<any>(this.APIUrl + '/Lists/GetConclusionTypes')
  }

  // LPR Correction Types
  getLPRorrectionTypesList(): Observable<any[]> {
    return this.http.get<any>(this.APIUrl + '/Lists/GetLPRCorrectionTypes')
  }

  // CCR Correction Types
  getCCRCorrectionTypesList(): Observable<any[]> {
    return this.http.get<any>(this.APIUrl + '/Lists/GetCCRCorrectionTypes')
  }

  // Inspections
  getInspectionDisplayColumns() {
    return this.http.get<any>(this.APIUrl + '/Inspection/GetInspectionDisplayColumns');
  }

  getInspections(): Observable<any[]> {
    return this.http.get<any>(this.APIUrl + '/Inspection')
  }

  addInspection(val: any) {
    return this.http.post(this.APIUrl + '/Inspection/', val);
  }

  askForBypass(id: string) {
     return this.http.get<any>(this.APIUrl + '/Inspection/AskForBypass/' + id);
  }

  updateInspection(val: any) {
    return this.http.put(this.APIUrl + '/Inspection/', val);
  }

  deleteInspection(val: any) {
    return this.http.delete(this.APIUrl + '/Inspection/' + val);
  }

  // Notifications
  getNotificationTypesList(): Observable<any[]> {
    return this.http.get<any>(this.APIUrl + '/Lists/GetNotificationTypes')
  }

  getNotificationDisplayColumns() {
    return this.http.get<any>(this.APIUrl + '/Notification/GetNotificationDisplayColumns');
  }

  getNotifications(): Observable<any[]> {
    return this.http.get<any>(this.APIUrl + '/Notification')
  }

  addNotification(val: any) {
    return this.http.post(this.APIUrl + '/Notification/', val);
  }

  updateNotification(val: any) {
    return this.http.put(this.APIUrl + '/Notification/', val);
  }

  updateNotificationIsHandled(val: any) {
    // TODO: This call is not calld from here!!
    return this.http.put(this.APIUrl + '/Notification/UpdateNotificationIsHandled/', val);
  }


  deleteNotification(val: any) {
    return this.http.delete(this.APIUrl + '/Notification/' + val);
  }

  // Type tables
  getTypeTableDisplayColumns(schema: string, table_name: string) {
    let url = this.APIUrl + '/GenericTypeTables/GetTypeTableDisplayColumns/' + schema + '/' + table_name;
    return this.http.get<any>(url);
  }

  getTypeTableEntries(schema: string, table_name: string): Observable<any[]> {
    return this.http.get<any>(this.APIUrl + '/GenericTypeTables/' + schema + '/' + table_name)
  }

  addTypeTableEntry(val: any) {
    return this.http.post(this.APIUrl + '/GenericTypeTables/', val);
  }

  updateTypeTableEntry(val: any) {
    return this.http.put(this.APIUrl + '/GenericTypeTables/', val);
  }

  deleteTypeTableEntry(id: string, schema: string, table_name: string) {
    return this.http.delete(this.APIUrl + '/GenericTypeTables/' + id + '/' + schema + '/' + table_name);
  }

}

