import { Component, OnInit, Input, Inject } from '@angular/core';
import { SharedService } from 'app/shared.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';


@Component({
  selector: 'app-add-edit-inspection',
  templateUrl: './add-edit-inspection.component.html',
  styleUrls: ['./add-edit-inspection.component.scss']
})
export class AddEditInspectionComponent implements OnInit {

  form: FormGroup;
  InspectionTypesList: any = [];
  LPRCorrectionTypesList: any = [];
  CCRCorrectionTypesList: any = [];
  ConclusionTypesList: any = [];

  id: number = -1;
  Identifier: string = "";
  WorkflowTypeId: number = -1;
  LPR: string = "";
  LPRCorrectionTypeId: number = 0;
  CCR: string = "";
  CCRCorrectionTypeId: number = 0;
  ManifestCode: string = "";
  DriverTagCode: string = "";
  ConclusionTypeId: number = 0;
  Arrival: Date = new Date();
  Leave: Date = new Date();
  End: Date = new Date();
  Remark: string = "";
  Created: Date = new Date();
  Updated: Date = new Date();
  isDeleted: boolean = false;

  constructor(private service: SharedService, private fb: FormBuilder, public dialogRef: MatDialogRef<AddEditInspectionComponent>, @Inject(MAT_DIALOG_DATA) public data: any) {
    this.id = this.data.obj.id;
    this.Identifier = this.data.obj.Identifier
    this.WorkflowTypeId = this.data.obj.WorkflowTypeId;
    this.LPR = this.data.obj.LPR;
    this.LPRCorrectionTypeId = this.data.obj.LPRCorrectionTypeId;
    this.CCR = this.data.obj.CCR;
    this.CCRCorrectionTypeId = this.data.obj.CCRCorrectionTypeId;
    this.ManifestCode = this.data.obj.ManifestCode;
    this.DriverTagCode = this.data.obj.DriverTagCode;
    this.ConclusionTypeId = this.data.obj.ConclusionTypeId;
    this.Arrival = this.data.obj.Arrival;
    this.Leave = this.data.obj.Leave;
    this.End = this.data.obj.End;
    this.Remark = this.data.obj.Remark;
    this.Created = this.data.obj.Created;
    this.Updated = this.data.obj.Updated;
    this.isDeleted = this.data.obj.isDeleted;
  }

  ngOnInit(): void {
    this.loadLists();

    this.form = this.fb.group({
      WorkflowTypeId: [''],
      LPR: ['', Validators.required],
      CCR: ['', Validators.required],
      ManifestCode: ['', Validators.required],
      ConclusionTypeId: [''],
      Remark: [''],
    });
  }

  loadLists() {
    this.loadInspectionTypesList();
    this.loadLPRCorrectionTypesList();
    this.loadCCRCorrectionTypesList();
    this.loadConclusionTypesList();

  }

  loadInspectionTypesList() {
    this.service.getInspectionTypesList().subscribe((types: any) => {
      this.InspectionTypesList = types;
    })
  }

  loadLPRCorrectionTypesList() {
    this.service.getLPRorrectionTypesList().subscribe((types: any) => {
      this.LPRCorrectionTypesList = types;
    })
  }

  loadCCRCorrectionTypesList() {
    this.service.getCCRCorrectionTypesList().subscribe((types: any) => {
      this.CCRCorrectionTypesList = types;
    })
  }

  loadConclusionTypesList() {
    this.service.getConclusionTypesList().subscribe((types: any) => {
      this.ConclusionTypesList = types;
    })
  }

  dismiss(): void {
    this.dialogRef.close();
  }

  save(): void {
    this.dialogRef.close(this.form.value);
  }

  public checkInputError = (controlName: string, errorName: string) => {
    return this.form.controls[controlName].hasError(errorName);
  }
}
