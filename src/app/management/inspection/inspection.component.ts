import { SharedService } from '../../shared.service';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import NotificationUtils from 'app/utils/notification-utils';
import { AppConfigService } from 'app/app-config.service';
import { MatDialog, MatDialogConfig, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Overlay } from '@angular/cdk/overlay';
import { AddEditInspectionComponent } from './add-edit-inspection/add-edit-inspection.component';

@Component({
  selector: 'app-inspection',
  templateUrl: './inspection.component.html',
  styleUrls: ['./inspection.component.scss']
})
export class InspectionComponent implements OnInit {

  constructor(private service: SharedService, private appConfigService: AppConfigService, public dialog: MatDialog, private overlay: Overlay) { }

  // Helpers
  page_title = "Inspections"
  edit_sufix = "Inspection";
  save_success = "Inspection is saved.";
  save_failiur = "Error while saving the inspection. Message: ";
  deletion_success = "Inspection deletion success.";
  deletion_failiure = "Error while deleting inspection. Message: ";
  dialog_width = "30%";

  defaultObj: any = {
    id: -1,
    Identifier: "",
    WorkflowTypeId: -1,
    LPR: "",
    LPRCorrectionTypeId: 0,
    CCR: "",
    CCRCorrectionTypeId: 0,
    ManifestCode: "",
    DriverTagCode: "",
    ConclusionTypeId: 0,
    Arrival: new Date(),
    Leave: new Date(),
    End: new Date(),
    Remark: "",
    Created: new Date(),
    Updated: new Date(),
    isDeleted: false
  }

  editObj: any = {
    title: "",
    obj: this.defaultObj,
  }
  
  onEdit: boolean = false;

  // UI display parametes 
  displayedColumns = [];
  totalRecord: number = 0;

  dataSource = new MatTableDataSource<any>();

  // Paginator
  pageSizeOptions: any;
  pageSize: number;


  ngOnInit(): void {
    this.pageSizeOptions = this.appConfigService.paginatorPageSizeOptions;
    this.pageSize = this.appConfigService.paginatorPageSize;


    this.refreshDataList();
    this.getDisplayColumns();
  }

  refreshDataList() {
    this.service.getInspections().subscribe(data => {
      this.dataSource = new MatTableDataSource(data);
      this.totalRecord = data.length;
    })
  }

  getDisplayColumns() {
    this.service.getInspectionDisplayColumns().subscribe(data => {
      this.displayedColumns = JSON.parse(data[0].ColumnNames);
    })
  }


  openAddModal() {
    this.onEdit = false;
    this.editObj.title = "Add " + this.edit_sufix;
    this.resetEditObject();
    this.openModal();
  }

  resetEditObject() {
    this.editObj.obj = this.defaultObj;
    this.onEdit = false;
  }

  openEditModal(obj: any) {
    this.onEdit = true;
    this.editObj.title = "Edit " + this.edit_sufix;
    this.editObj.obj = obj;
    this.openModal();
  }
  openModal() {
    const dialogConfig = new MatDialogConfig();
    const scrollStrategy = this.overlay.scrollStrategies.reposition();
    dialogConfig.data = this.editObj;
    dialogConfig.scrollStrategy = scrollStrategy;
    dialogConfig.width = this.dialog_width;
    const dialogRef = this.dialog.open(AddEditInspectionComponent, dialogConfig);

    dialogRef.afterClosed().subscribe((data) => {
      if (data) {
        if (data.LPR != this.editObj.obj.LPR) {
          this.editObj.obj.LPR = data.LPR;
          this.editObj.obj.LPRCorrectionTypeId = 2; // MANUAL
        }

        if (data.CCR != this.editObj.obj.CCR) {
          this.editObj.obj.CCR = data.CCR;
          this.editObj.obj.CCRCorrectionTypeId = 2; // MANUAL
        }

        this.editObj.obj.WorkflowTypeId = data.WorkflowTypeId;
        this.editObj.obj.Remark = data.Remark;
        this.editObj.obj.ConclusionTypeId = data.ConclusionTypeId;
        this.editObj.obj.ManifestCode = data.ManifestCode;

        this.saveObject();

      }
    });

  }

  saveObject() {

    if (this.onEdit) {
      this.saveOnEdit();
    }
    else {
      this.saveOnAdd();
    }

    this.resetEditObject();
  }

  saveOnEdit() {
    this.service.updateInspection(this.getEditObjForSave()).subscribe(res => {
      if (res && res === 'Success') {
        NotificationUtils.showNotification(this.save_success, 'success');
        this.refreshDataList();
      }
      else {
        NotificationUtils.showNotification(this.save_failiur + res.toString(), 'danger');
      }
    });
  }

  saveOnAdd() {
    this.service.addInspection(this.getEditObjForSave()).subscribe(res => {
      if (res && res === 'Success') {
        NotificationUtils.showNotification(this.save_success, 'success');
        this.refreshDataList();
      }
      else {
        NotificationUtils.showNotification(this.save_failiur + res.toString(), 'danger');
      }
    });
  }

getEditObjForSave(){
  let o = this.editObj.obj;
  
  return {
    id: o.id,
    Identifier: o.Identifier,
    WorkflowTypeId: o.WorkflowTypeId,
    LPR: o.LPR,
    LPRCorrectionTypeId: o.LPRCorrectionTypeId,
    CCR: o.CCR,
    CCRCorrectionTypeId: o.CCRCorrectionTypeId,
    ManifestCode: o.ManifestCode,
    DriverTagCode: o.DriverTagCode,
    ConclusionTypeId: o.ConclusionTypeId,
    Arrival: o.Arrival,
    Leave: o.Leave,
    End: o.End,
    Remark: o.Remark,
    isDeleted: o.isDeleted
  }
}

  deleteObject(item: any) {
    let that = this;
    NotificationUtils.openConfirmBox("Delete?",
      function () {
        that.service.deleteInspection(item.id).subscribe(res => {
          if (res && res === 'Success') {
            NotificationUtils.showNotification(that.deletion_success, 'success');
            that.refreshDataList();
          }
          else {
            NotificationUtils.showNotification(that.deletion_failiure + res.toString(), 'danger');
          }

        });
      },
      function () {

      }, 'Confirm Deletion', 'Delete', 'Cancel');
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

}
