import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NotificatoinComponent } from './notificatoin.component';

describe('NotificatoinComponent', () => {
  let component: NotificatoinComponent;
  let fixture: ComponentFixture<NotificatoinComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NotificatoinComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NotificatoinComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
