import { SharedService } from '../../shared.service';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import NotificationUtils from 'app/utils/notification-utils';
import { AppConfigService } from 'app/app-config.service';
import { MatDialog, MatDialogConfig, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Overlay } from '@angular/cdk/overlay';
import { AddEditNotificationComponent } from './add-edit-notification/add-edit-notification.component';

@Component({
  selector: 'app-notificatoin',
  templateUrl: './notificatoin.component.html',
  styleUrls: ['./notificatoin.component.scss']
})
export class NotificatoinComponent implements OnInit {

  constructor(private service: SharedService, private appConfigService: AppConfigService, public dialog: MatDialog, private overlay: Overlay) { }

  // Helpers
  page_title = "Notifications"
  edit_sufix = "Notification";
  save_success = "Notification is saved.";
  save_failiur = "Error while saving the notification. Message: ";
  deletion_success = "Notification deletion success.";
  deletion_failiure = "Error while deleting notification. Message: ";
  dialog_width = "50%";

  defaultObj: any = {
    id: -1,
    WorkflowTypeId: -1,
    Subject: "",
    Message: "",
    JsonData: "",
    SourceUserId: 0,
    TargetUserId: 0,
    Reply: "",
    IsHandled: false,
    ConclusionTypeId: 0,
    Created: new Date(),
    Updated: new Date(),
    isDeleted: false
  }

  editObj: any = {
    title: "",
    obj: this.defaultObj,
  }

  onEdit: boolean = false;

  // UI display parametes 
  displayedColumns = [];
  totalRecord: number = 0;

  dataSource = new MatTableDataSource<any>();

  // Paginator
  pageSizeOptions: any;
  pageSize: number;


  ngOnInit(): void {
    this.pageSizeOptions = this.appConfigService.paginatorPageSizeOptions;
    this.pageSize = this.appConfigService.paginatorPageSize;


    this.refreshDataList();
    this.getDisplayColumns();
  }

  refreshDataList() {
    this.service.getNotifications().subscribe(data => {
      this.dataSource = new MatTableDataSource(data);
      this.totalRecord = data.length;
    })
  }

  getDisplayColumns() {
    this.service.getNotificationDisplayColumns().subscribe(data => {
      this.displayedColumns = JSON.parse(data[0].ColumnNames);
    })
  }


  openAddModal() {
    this.onEdit = false;
    this.editObj.title = "Add " + this.edit_sufix;
    this.resetEditObject();
    this.openModal();
  }

  resetEditObject() {
    this.editObj.obj = this.defaultObj;
    this.onEdit = false;
  }

  openEditModal(obj: any) {
    this.onEdit = true;
    this.editObj.title = "Edit " + this.edit_sufix;
    this.editObj.obj = obj;
    this.openModal();
  }

  openModal() {
    const dialogConfig = new MatDialogConfig();
    const scrollStrategy = this.overlay.scrollStrategies.reposition();
    dialogConfig.data = this.editObj;
    dialogConfig.scrollStrategy = scrollStrategy;
    dialogConfig.width = this.dialog_width;
    const dialogRef = this.dialog.open(AddEditNotificationComponent, dialogConfig);

    dialogRef.afterClosed().subscribe((data) => {
      if (data) {
        this.saveObject(data);

      }
    });

  }

  saveObject(data: any) {

    if (this.onEdit) {
      data.Id = this.editObj.obj.Id;
      this.saveOnEdit(data);
    }
    else {
      this.saveOnAdd(data);
    }

    this.resetEditObject();
  }

  saveOnEdit(val: any) {
    let that = this;
    this.service.updateNotification(val).subscribe(res => {
      if (res && res === 'Success') {
        if (val.IsHandled === false && val.Reply != '') {
          // Incases that is handled is false and the data has reply, it is the user's choiceto mark it as NOT Handled so update it specifically
          // TODO: This call is not calld from here!! It is not working. Note that this code is reachable.
          this.service.updateNotificationIsHandled({ Id: val.Id, IsHandled: val.IsHandled });
        }

        NotificationUtils.showNotification(this.save_success, 'success');
        this.refreshDataList();
      }
      else {
        NotificationUtils.showNotification(this.save_failiur + res.toString(), 'danger');
      }
    });


  }

  saveOnAdd(val: any) {
    this.service.addNotification(val).subscribe(res => {
      if (res && res === 'Success') {
        NotificationUtils.showNotification(this.save_success, 'success');
        this.refreshDataList();
      }
      else {
        NotificationUtils.showNotification(this.save_failiur + res.toString(), 'danger');
      }
    });
  }

  deleteObject(item: any) {
    let that = this;
    NotificationUtils.openConfirmBox("Delete?",
      function () {
        that.service.deleteNotification(item.id).subscribe(res => {
          if (res && res === 'Success') {
            NotificationUtils.showNotification(that.deletion_success, 'success');
            that.refreshDataList();
          }
          else {
            NotificationUtils.showNotification(that.deletion_failiure + res.toString(), 'danger');
          }

        });
      },
      function () {

      }, 'Confirm Deletion', 'Delete', 'Cancel');
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }


}
