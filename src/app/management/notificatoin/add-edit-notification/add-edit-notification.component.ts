import { Component, OnInit, Input, Inject } from '@angular/core';
import { SharedService } from 'app/shared.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';


@Component({
  selector: 'app-add-edit-notification',
  templateUrl: './add-edit-notification.component.html',
  styleUrls: ['./add-edit-notification.component.scss']
})
export class AddEditNotificationComponent implements OnInit {

  notificationTypesList: any = [];
  usersDisplayList: any = [];

  form: FormGroup;
  Id: number = -1
  NotificationTypeId: Number = -1;
  Subject: string = "";
  Message: string = "";
  JsonData: string = "";
  SourceUserId: Number = -1;
  TargetUserId: Number = -1;
  Reply: string = "";
  IsHandled: boolean = true;
  IsReplyable: boolean = true;

  constructor(private service: SharedService, private fb: FormBuilder, public dialogRef: MatDialogRef<AddEditNotificationComponent>, @Inject(MAT_DIALOG_DATA) public data: any) {
    this.Id = this.data.obj.Id;
    this.NotificationTypeId = this.data.obj.NotificationTypeId;
    this.Subject = this.data.obj.Subject;
    this.Message = this.data.obj.Message;
    this.JsonData = JSON.stringify(this.data.obj.JsonData);
    this.SourceUserId = this.data.obj.SourceUserId;
    this.TargetUserId = this.data.obj.TargetUserId;
    this.Reply = this.data.obj.Reply;
    this.IsHandled = this.data.obj.IsHandled;
    this.IsReplyable = this.data.obj.IsReplyable;
  }


  ngOnInit(): void {

    this.loadNotificationTypesList();
    this.loadUsersDisplayList();


    this.form = this.fb.group({
      NotificationTypeId: ['', Validators.required],
      Subject: ['', Validators.required],
      Message: ['', Validators.required],
      JsonData: [''],
      SourceUserId: [''],
      TargetUserId: [''],
      Reply: [''],
      IsReplyable: [1],
      IsHandled: [1],
    });

  }

  loadUsersDisplayList() {
    this.service.getNotificationTypesList().subscribe((types: any) => {
      this.notificationTypesList = types;
    })
  }

  loadNotificationTypesList() {
    this.service.getUserDisplayList().subscribe((types: any) => {
      this.usersDisplayList = types;
    })
  }

  dismiss(): void {
    this.dialogRef.close();
  }

  save() {
    this.dialogRef.close(this.form.value);
  }

  public checkInputError = (controlName: string, errorName: string) => {
    return this.form.controls[controlName].hasError(errorName);
  }


}
