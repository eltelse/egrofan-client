import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GenericTypeCrudComponent } from './generic-type-crud.component';

describe('GenericTypeCrudComponent', () => {
  let component: GenericTypeCrudComponent;
  let fixture: ComponentFixture<GenericTypeCrudComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GenericTypeCrudComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GenericTypeCrudComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
