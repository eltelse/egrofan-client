import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddAditGenericTypeCrudComponent } from './add-adit-generic-type-crud.component';

describe('AddAditGenericTypeCrudComponent', () => {
  let component: AddAditGenericTypeCrudComponent;
  let fixture: ComponentFixture<AddAditGenericTypeCrudComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddAditGenericTypeCrudComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddAditGenericTypeCrudComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
