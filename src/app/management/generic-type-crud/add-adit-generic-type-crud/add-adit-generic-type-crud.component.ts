import { Component, OnInit, Input, Inject } from '@angular/core';
import { SharedService } from 'app/shared.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';


@Component({
  selector: 'app-add-adit-generic-type-crud',
  templateUrl: './add-adit-generic-type-crud.component.html',
  styleUrls: ['./add-adit-generic-type-crud.component.scss']
})
export class AddAditGenericTypeCrudComponent implements OnInit {

  form: FormGroup;
  id: number = -1;
  Name: string = "";
  Description: string = "";

  constructor(private service: SharedService, private fb: FormBuilder, public dialogRef: MatDialogRef<AddAditGenericTypeCrudComponent>, @Inject(MAT_DIALOG_DATA) public data: any) {
    this.id = this.data.obj.id;
    this.Name = this.data.obj.Name;
    this.Description = this.data.obj.Description;

  }

  ngOnInit(): void {
    this.form = this.fb.group({
      Name: ['', Validators.required],
      Description: [''],
    });
  }

  dismiss(): void {
    this.dialogRef.close();
  }

  save(): void {
    this.dialogRef.close(this.form.value);
  }

  public checkInputError = (controlName: string, errorName: string) => {
    return this.form.controls[controlName].hasError(errorName);
  }

}
