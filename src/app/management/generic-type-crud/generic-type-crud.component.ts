import { SharedService } from '../../shared.service';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import NotificationUtils from 'app/utils/notification-utils';
import { AppConfigService } from 'app/app-config.service';
import { MatDialog, MatDialogConfig, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Overlay } from '@angular/cdk/overlay';
import { Location, LocationStrategy, PathLocationStrategy } from '@angular/common';
import { AddAditGenericTypeCrudComponent } from './add-adit-generic-type-crud/add-adit-generic-type-crud.component';


@Component({
  selector: 'app-generic-type-crud',
  templateUrl: './generic-type-crud.component.html',
  styleUrls: ['./generic-type-crud.component.scss']
})
export class GenericTypeCrudComponent implements OnInit {

  constructor(location: Location, private service: SharedService, private appConfigService: AppConfigService, public dialog: MatDialog, private overlay: Overlay) {
    this.location = location;
  }

  location: Location;

  save_success = "Data was successfully saved";
  save_failiur = "Error while saving the data. Message: ";
  deletion_success = "Deletion success.";
  deletion_failiure = "Error while deleting. Message: ";

  db_schema = "type";
  table_name = "";
  page_title = "";
  dialog_width = "30%";

  defaultObj: any = {
    Id: -1,
    Name: "",
    Description: "",
    Created: new Date(),
    Updated: new Date(),
    isDeleted: false
  }

  editObj: any = {
    title: "",
    obj: this.defaultObj,
  }
  onEdit: boolean = false;

  // UI display parametes 
  displayedColumns = [];
  totalRecord: number = 0;

  dataSource = new MatTableDataSource<any>();

  // Paginator
  pageSizeOptions: any;
  pageSize: number;

  ngOnInit(): void {
    this.pageSizeOptions = this.appConfigService.paginatorPageSizeOptions;
    this.pageSize = this.appConfigService.paginatorPageSize;

    this.setTableName()
    this.refreshDataList();
    this.getDisplayColumns();
  }


  setTableName() {
    var title = this.location.prepareExternalUrl(this.location.path());
    if (title.charAt(1) === '/') {
      title = title.slice(2);
      this.table_name = title;
      this.page_title = title;
    }

    if (title.charAt(0) === '#') {
      title = title.slice(1);
      this.table_name = title;
      this.page_title = title;
    }
  }

  refreshDataList() {
    this.service.getTypeTableEntries(this.db_schema, this.table_name).subscribe(data => {
      this.dataSource = new MatTableDataSource(data);
      this.totalRecord = data.length;
    })
  }

  getDisplayColumns() {
    this.service.getTypeTableDisplayColumns(this.db_schema, this.table_name).subscribe(data => {
      this.displayedColumns = JSON.parse(data[0].ColumnNames);
    })
  }

  openAddModal() {
    this.editObj.title = "Add " + this.table_name + " Entry";
    this.resetEditObject();
    this.openModal();

  }
  resetEditObject() {
    this.onEdit = false;
    this.editObj.obj = this.defaultObj;
  }

  openModal() {
    const dialogConfig = new MatDialogConfig();
    const scrollStrategy = this.overlay.scrollStrategies.reposition();
    dialogConfig.data = this.editObj;
    dialogConfig.scrollStrategy = scrollStrategy;
    dialogConfig.width = this.dialog_width;
    const dialogRef = this.dialog.open(AddAditGenericTypeCrudComponent, dialogConfig);

    dialogRef.afterClosed().subscribe((data) => {
      if (data) {
        this.saveObject(data);
      }
    });
  }

  openEditModal(obj: any) {
    this.onEdit = true;
    this.editObj.title = "Edit " + this.table_name + " Entry";
    this.editObj.obj = obj;

    this.openModal();
  }


  saveObject(data: any) {

    if (this.onEdit) {
      let obj = {
        Id: this.editObj.obj.Id,
        Name: data.Name,
        Description: data.Description,
        Schema: this.db_schema,
        TableName: this.table_name,
      }
      this.saveOnEdit(obj);
    }
    else {
      let obj = {
        Name: data.Name,
        Description: data.Description,
        Schema: this.db_schema,
        TableName: this.table_name,
      }
      this.saveOnAdd(obj);
    }

    this.resetEditObject();
  }

  saveOnEdit(obj: any) {
    this.service.updateTypeTableEntry(obj).subscribe(res => {
      if (res && res === 'Success') {
        NotificationUtils.showNotification(this.save_success, 'success');
        this.refreshDataList();
      }
      else {
        NotificationUtils.showNotification(this.save_failiur + res.toString(), 'danger');
      }
    });
  }

  saveOnAdd(obj: any) {
    this.service.addTypeTableEntry(obj).subscribe(res => {
      if (res && res === 'Success') {
        NotificationUtils.showNotification(this.save_success, 'success');
        this.refreshDataList();
      }
      else {
        NotificationUtils.showNotification(this.save_failiur + res.toString(), 'danger');
      }
    });
  }

  deleteObject(item: any) {
    let that = this;
    NotificationUtils.openConfirmBox("Delete?",
      function () {
        that.service.deleteTypeTableEntry(item.Id, that.db_schema, that.table_name).subscribe(res => {
          if (res && res === 'Success') {
            NotificationUtils.showNotification(that.deletion_success, 'success');
            that.refreshDataList();
          }
          else {
            NotificationUtils.showNotification(that.deletion_failiure + res.toString(), 'danger');
          }

        });
      },
      function () {

      }, 'Confirm Deletion', 'Delete', 'Cancel');
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

}
