import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { UtilsModule } from 'app/utils/utils.module';
import { ComponentsModule } from 'app/components/components.module';

import { AddEditWorkStationComponent } from './work-station/add-edit-work-station/add-edit-work-station.component';
import { WorkStationComponent } from './work-station/work-station.component';
import { UserListComponent } from './user-list/user-list.component';
import { AddEditUserComponent } from './user-list/add-edit-user/add-edit-user.component';
import { InspectionComponent } from './inspection/inspection.component';
import { AddEditInspectionComponent } from './inspection/add-edit-inspection/add-edit-inspection.component';
import { GenericTypeCrudComponent } from './generic-type-crud/generic-type-crud.component';
import { AddAditGenericTypeCrudComponent } from './generic-type-crud/add-adit-generic-type-crud/add-adit-generic-type-crud.component';
import { VehicleComponent } from './vehicle/vehicle.component';
import { AddEditVehicleComponent } from './vehicle/add-edit-vehicle/add-edit-vehicle.component';
import { NotificatoinComponent } from './notificatoin/notificatoin.component';
import { AddEditNotificationComponent } from './notificatoin/add-edit-notification/add-edit-notification.component';



@NgModule({
  declarations: [
    WorkStationComponent,
    AddEditWorkStationComponent,
    UserListComponent,
    AddEditUserComponent,
    InspectionComponent,
    AddEditInspectionComponent,
    GenericTypeCrudComponent,
    AddAditGenericTypeCrudComponent,
    VehicleComponent,
    AddEditVehicleComponent,
    NotificatoinComponent,
    AddEditNotificationComponent,    

  ],
  imports: [
    CommonModule,
    FormsModule,
    BrowserModule,
    HttpClientModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    NgbModule,

    UtilsModule, // Angulare Material + More

    ComponentsModule,

  ],
  exports: [
    WorkStationComponent,
    AddEditWorkStationComponent,
    UserListComponent,
    AddEditUserComponent,
    InspectionComponent,
    AddEditInspectionComponent,
    GenericTypeCrudComponent,
    AddAditGenericTypeCrudComponent,
    VehicleComponent,
    AddEditVehicleComponent,
    NotificatoinComponent,
    AddEditNotificationComponent,    
  ]
})
export class ManagementModule { }
