import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddEditWorkStationComponent } from './add-edit-work-station.component';

describe('AddEditWorkStationComponent', () => {
  let component: AddEditWorkStationComponent;
  let fixture: ComponentFixture<AddEditWorkStationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddEditWorkStationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddEditWorkStationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
