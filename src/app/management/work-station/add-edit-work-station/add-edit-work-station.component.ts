import { Component, OnInit, Input, Inject } from '@angular/core';
import { SharedService } from 'app/shared.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';


@Component({
  selector: 'app-add-edit-work-station',
  templateUrl: './add-edit-work-station.component.html',
  styleUrls: ['./add-edit-work-station.component.css']
})
export class AddEditWorkStationComponent implements OnInit {

  workStationTypesList: any = [];

  form: FormGroup;
  Id: number = -1
  Name: string = "";
  Description: string = "";
  IP: string = "";
  MAC: string = "";
  IsActive: boolean = true;
  WorkStationTypeId: number = -1;

  constructor(private service: SharedService, private fb: FormBuilder, public dialogRef: MatDialogRef<AddEditWorkStationComponent>, @Inject(MAT_DIALOG_DATA) public data: any) {
    this.Id = this.data.obj.Id;
    this.Name = this.data.obj.Name;
    this.Description = this.data.obj.Description;
    this.IP = this.data.obj.IP;
    this.MAC = this.data.obj.MAC;
    this.IsActive = this.data.obj.IsActive;
    this.WorkStationTypeId = this.data.obj.WorkStationTypeId;
  }


  ngOnInit(): void {

    this.loadWorkStationTypesList();

    this.form = this.fb.group({
      Name: ['', Validators.required],
      Description: [''],
      IP: [''],
      MAC: [''],
      IsActive: [''],
      WorkStationTypeId: [''],
    });
  }

  loadWorkStationTypesList() {
    this.service.getWorkStationsTypes().subscribe((types: any) => {
      this.workStationTypesList = types;
    })
  }

  dismiss(): void {
    this.dialogRef.close();
  }

  save() {
    this.dialogRef.close(this.form.value);
  }

  public checkInputError = (controlName: string, errorName: string) => {
    return this.form.controls[controlName].hasError(errorName);
  }
}
