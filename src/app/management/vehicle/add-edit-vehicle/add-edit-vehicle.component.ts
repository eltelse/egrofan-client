import { Component, OnInit, Input, Inject } from '@angular/core';
import { SharedService } from 'app/shared.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-edit-vehicle',
  templateUrl: './add-edit-vehicle.component.html',
  styleUrls: ['./add-edit-vehicle.component.css']
})
export class AddEditVehicleComponent implements OnInit {

  VehicleTypesList: any = [];
  
  form: FormGroup;  
  Id: number = -1
  LicensePlateNumber: string = "";
  VehicleTypeId: number = -1;
  Remark: string = "";
  
  constructor(private service: SharedService, private fb: FormBuilder, public dialogRef: MatDialogRef<AddEditVehicleComponent>, @Inject(MAT_DIALOG_DATA) public data: any) { 
    this.Id = this.data.obj.Id;
    this.LicensePlateNumber = this.data.obj.LicensePlateNumber;
    this.VehicleTypeId = this.data.obj.VehicleTypeId;
    this.Remark = this.data.obj.Remark;
  }


  ngOnInit(): void {
    this.loadVehicleTypesList();

    this.form = this.fb.group({
      LicensePlateNumber: ['', Validators.required],
      VehicleTypeId: [-1, Validators.required],
      Remark: [''],
    });

  }

  loadVehicleTypesList() {
    this.service.getVehicleTypesList().subscribe((types: any) => {
      this.VehicleTypesList = types;
    })
  }

  dismiss(): void {
    this.dialogRef.close();
  }

  save() {
    this.dialogRef.close(this.form.value);
  }

  public checkInputError = (controlName: string, errorName: string) => {
    return this.form.controls[controlName].hasError(errorName);
  }
}
