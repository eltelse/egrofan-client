import { SharedService } from '../../shared.service';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import NotificationUtils from 'app/utils/notification-utils';
import { AppConfigService } from 'app/app-config.service';
import { MatDialog, MatDialogConfig, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Overlay } from '@angular/cdk/overlay';
import { AddEditVehicleComponent } from './add-edit-vehicle/add-edit-vehicle.component';


@Component({
  selector: 'app-vehicle',
  templateUrl: './vehicle.component.html',
  styleUrls: ['./vehicle.component.css']
})
export class VehicleComponent implements OnInit {

  constructor(private service: SharedService, private appConfigService: AppConfigService, public dialog: MatDialog, private overlay: Overlay) { }

  page_title = "Vehicles";
  edit_sufix = "Vehicle";
  save_success = "Vehicle was successfully saved";
  save_failiur = "Error while saving a vehicle. Message: ";
  deletion_success = "Deletion success.";
  deletion_failiure = "Error while deleting the vehicle. Message: ";
  dialog_width = "30%";

  defaultObj: any = {
    Id: -1,
    LicensePlateNumber: "",
    VehicleTypeId: -1,
    Remark: "",
  };

  editObj: any = {
    title: "",
    obj: this.defaultObj,
  }
  onEdit: boolean = false;

    // UI display parametes 
    displayedColumns = [];
    totalRecord: number = 0;
  
    dataSource = new MatTableDataSource<any>();
  
    // Paginator
    pageSizeOptions: any;
    pageSize: number;

  ngOnInit(): void {
    this.pageSizeOptions = this.appConfigService.paginatorPageSizeOptions;
    this.pageSize = this.appConfigService.paginatorPageSize;

    this.refreshDataList();
    this.getDisplayColumns();

  }

  ngAfterViewInit() {
  }


  refreshDataList() {
    this.service.getVehicleList().subscribe(data => {
      this.dataSource = new MatTableDataSource(data);
      this.totalRecord = data.length;
    })
  }

  getDisplayColumns() {
    this.service.getVehicleDisplayColumns().subscribe(data => {
      this.displayedColumns = JSON.parse(data[0].ColumnNames);
    })
  }


  openAddModal() {
    this.onEdit = false;
    this.editObj.title = "Add " + this.edit_sufix;
    this.resetEditObject();
    this.openModal();
  }

  resetEditObject() {
    this.editObj.obj = this.defaultObj;
    this.onEdit = false;
  }

  openEditModal(obj: any) {
    this.editObj.obj = obj;
    this.editObj.title = "Edit " + this.edit_sufix;
    this.onEdit = true;
    this.openModal();

  }

  openModal() {
    const dialogConfig = new MatDialogConfig();
    const scrollStrategy = this.overlay.scrollStrategies.reposition();
    dialogConfig.data = this.editObj;
    dialogConfig.scrollStrategy = scrollStrategy;
    dialogConfig.width = this.dialog_width;
    const dialogRef = this.dialog.open(AddEditVehicleComponent, dialogConfig);

    dialogRef.afterClosed().subscribe((data) => {
      if (data) {
        this.saveObject(data);
      }
    });
  }


  saveObject(data: any) {

    if (this.onEdit) {
      data.Id = this.editObj.obj.id;
      this.saveOnEdit(data);
    }
    else {
      this.saveOnAdd(data);
    }

    this.resetEditObject();
  }


  saveOnEdit(val: any) {
    val.Id = this.editObj.obj.Id;
    this.service.updateVehicle(val).subscribe(res => {
      if (res && res === 'Success') {
        NotificationUtils.showNotification(this.save_success, 'success');
        this.refreshDataList();
      }
      else {
        NotificationUtils.showNotification(this.save_failiur + res.toString(), 'danger');
      }
    });
  }

  saveOnAdd(val: any) {
    this.service.addVehicle(val).subscribe(res => {
      if (res && res === 'Success') {
        NotificationUtils.showNotification(this.save_success, 'success');
        this.refreshDataList();
      }
      else {
        NotificationUtils.showNotification(this.save_failiur + res.toString(), 'danger');
      }
    });
  }

  deleteObject(item: any) {
    let that = this;
    NotificationUtils.openConfirmBox("Delete?",
      function () {
        that.service.deleteVehicle(item.Id).subscribe(res => {
          if (res && res === 'Success') {
            NotificationUtils.showNotification(that.deletion_success, 'success');
            that.refreshDataList();
          }
          else {
            NotificationUtils.showNotification(that.deletion_failiure + res.toString(), 'danger');
          }

        });
      },
      function () {

      }, 'Confirm Vehicle Deletion', 'Delete', 'Cancel');
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }


}
