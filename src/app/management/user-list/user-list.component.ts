import { SharedService } from '../../shared.service';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import NotificationUtils from 'app/utils/notification-utils';
import { AppConfigService } from 'app/app-config.service';
import { MatDialog, MatDialogConfig, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Overlay } from '@angular/cdk/overlay';
import { AddEditUserComponent } from './add-edit-user/add-edit-user.component';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {

  constructor(private service: SharedService, private appConfigService: AppConfigService, public dialog: MatDialog, private overlay: Overlay) { }

  page_title = "Users"
  edit_sufix = "User";
  save_success = "User was successfully saved";
  save_failiur = "Error while saving user data. Message: ";
  deletion_success = "Deletion success.";
  deletion_failiure = "Error while deleting the user. Message: ";
  dialog_width = "30%";

  defaultObj: any = {
    Id: -1,
    FirstName: "",
    LastName: "",
    Email: "",
    Password: "",
  };

  editObj: any = {
    title: "",
    obj: this.defaultObj,
  }
  onEdit: boolean = false;

  // UI display parametes 
  displayedColumns = [];
  totalRecord: number = 0;

  dataSource = new MatTableDataSource<any>();

  // Paginator
  pageSizeOptions: any;
  pageSize: number;

  ngOnInit(): void {
    this.pageSizeOptions = this.appConfigService.paginatorPageSizeOptions;
    this.pageSize = this.appConfigService.paginatorPageSize;

    this.refreshDataList();
    this.getDisplayColumns();
  }

  ngAfterViewInit() {
  }

  refreshDataList() {
    this.service.getUsersList().subscribe(data => {
      this.dataSource = new MatTableDataSource(data);
      this.totalRecord = data.length;
    })
  }

  getDisplayColumns() {
    this.service.getUserDisplayColumns().subscribe(data => {
      this.displayedColumns = JSON.parse(data[0].ColumnNames);
    })
  }

  openAddModal() {
    this.onEdit = false;
    this.editObj.title = "Add " + this.edit_sufix;
    this.resetEditObject();
    this.openModal();
  }

  resetEditObject() {
    this.editObj.obj = this.defaultObj;
    this.onEdit = false;
  }

  openEditModal(obj: any) {
    this.editObj.obj = obj;
    this.editObj.title = "Edit " + this.edit_sufix;
    this.onEdit = true;
    this.openModal();

  }

  openModal() {
    const dialogConfig = new MatDialogConfig();
    const scrollStrategy = this.overlay.scrollStrategies.reposition();
    dialogConfig.data = this.editObj;
    dialogConfig.scrollStrategy = scrollStrategy;
    dialogConfig.width = this.dialog_width;
    const dialogRef = this.dialog.open(AddEditUserComponent, dialogConfig);

    dialogRef.afterClosed().subscribe((data) => {
      if (data) {
        this.saveObject(data);
      }
    });
  }

  saveObject(data: any) {

    if (this.onEdit) {
      data.Id = this.editObj.obj.id;
      this.saveOnEdit(data);
    }
    else {
      this.saveOnAdd(data);
    }

    this.resetEditObject();
  }

  saveOnEdit(val: any) {
    this.service.updateUserDetails(val).subscribe(res => {
      if (res && res === 'Success') {
        NotificationUtils.showNotification(this.save_success, 'success');
        this.refreshDataList();
      }
      else {
        NotificationUtils.showNotification(this.save_failiur + res.toString(), 'danger');
      }
    });
  }

  saveOnAdd(val: any) {
    this.service.addUser(val).subscribe(res => {
      if (res && res === 'Success') {
        NotificationUtils.showNotification(this.save_success, 'success');
        this.refreshDataList();
      }
      else {
        NotificationUtils.showNotification(this.save_failiur + res.toString(), 'danger');
      }
    });
  }

  deleteObject(item: any) {
    let that = this;
    NotificationUtils.openConfirmBox("Delete?",
      function () {
        that.service.deleteUser(item.id).subscribe(res => {
          if (res && res === 'Success') {
            NotificationUtils.showNotification(that.deletion_success, 'success');
            that.refreshDataList();
          }
          else {
            NotificationUtils.showNotification(that.deletion_failiure + res.toString(), 'danger');
          }

        });
      },
      function () {

      }, 'Confirm User Deletion', 'Delete', 'Cancel');
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

}
