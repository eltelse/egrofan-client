import { Component, OnInit, Input, Inject } from '@angular/core';
import { SharedService } from 'app/shared.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';


@Component({
  selector: 'app-edit-user',
  templateUrl: './add-edit-user.component.html',
  styleUrls: ['./add-edit-user.component.css']
})
export class AddEditUserComponent implements OnInit {

  form: FormGroup;
  Id: number = -1
  FirstName: string = "";
  LastName: string = "";
  Email: string = "";
  Password: string = "";
  hidePassword: boolean = true;

  constructor(private service: SharedService, private fb: FormBuilder, public dialogRef: MatDialogRef<AddEditUserComponent>, @Inject(MAT_DIALOG_DATA) public data: any) {
    this.Id = this.data.obj.Id;
    this.FirstName = this.data.obj.FirstName;
    this.LastName = this.data.obj.LastName;
    this.Email = this.data.obj.Email;
    this.Password = this.data.obj.Password;

  }

  ngOnInit(): void {

    this.form = this.fb.group({
      FirstName: ['', Validators.required],
      LastName: ['', Validators.required],
      Email: ['', [, Validators.required, Validators.email]],
      Password: ['', Validators.required],
    });
  }

  dismiss(): void {
    this.dialogRef.close();
  }

  save() {
    this.dialogRef.close(this.form.value);
  }

  public checkInputError = (controlName: string, errorName: string) => {
    return this.form.controls[controlName].hasError(errorName);
  }

}
