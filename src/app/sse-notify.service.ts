import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Subject } from 'rxjs';
import { AppConfigService } from './app-config.service';

@Injectable({
  providedIn: 'root'
})
export class ServerSideNotifyService {

  constructor(private http: HttpClient, private appConfigService: AppConfigService) { }

  // See: https://ramya-bala221190.medium.com/simple-example-to-demonstrate-server-sent-events-in-angular-and-node-ea9979b6e19
  /*
    See all:

  */
  evs: EventSource;
  //private subj = new BehaviorSubject([]);
  
  private checkin_subject = new Subject();
  private bypass_subject = new Subject();
  
  getCheckinAsObservable() {
    return this.checkin_subject.asObservable();
  }

  getBypassAsObservable() {
    return this.bypass_subject.asObservable();
  }

  notificationsListenStart() {
    console.log('SSE Registering: ' + this.appConfigService.sseNotificationUrl);
    let bp_subject = this.bypass_subject;
    let ci_subject = this.checkin_subject;

    if (typeof (EventSource) !== 'undefined' && !this.evs) {
      console.log('Instantiating EventSource.');
      this.evs = new EventSource(this.appConfigService.sseNotificationUrl);
      this.evs.onopen = function (e) {
        console.log('SSE opening connection to service. Ready State: ' + this.readyState);
      };
      // This handler is fallback that is called when event name is not set.
      this.evs.onmessage = function (e) {
        console.log("SSE message received by 'onmessage' listener. Ready State: " + this.readyState);
        ci_subject.next(JSON.parse(e.data));
        bp_subject.next(JSON.parse(e.data));
      };
      // bypass notification 
      this.evs.addEventListener('bypass', e => {
        console.log("SSE message received by 'bypass' listener");
        bp_subject.next(JSON.parse(e['data']));
      });
      // Check in pop up notification
      this.evs.addEventListener('checkin', e => {
        console.log("SSE message received by 'bypass' listener");
        ci_subject.next(JSON.parse(e['data']));
      });

      this.evs.onerror = function (e) {
        // Note!! Couln't find that e is a real object. Documentation does not say that it sould be recived at all.
        // In any case ther is no e.data or e.message in it. 
        console.log("SSE 'onerror' message recived,");


        if (this.readyState == 0) { // 0 is 'connecting' / 1 is 'connected' 2 meens 'connection is closed'.
          console.log("Reconnecting.. (by default EventSource reconnects after a few seconds, but the imterval can be changed from the server by sending a 'retry' message).");
        }
      };
    }
  }
  stopExchangeUpdates() {
    this.evs.close();
  }

}
