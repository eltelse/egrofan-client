import { Component, Input, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { SharedService } from '../shared.service';

@Component({
  selector: 'app-driver-tag',
  templateUrl: './driver-tag.component.html',
  styleUrls: ['./driver-tag.component.css']
})
export class DriverTagComponent implements OnInit {

  constructor(private service:SharedService,private router:Router) { }

  @Input() driverTag:any;
  
  ModalTitle:string = "";
  ActivateAddDriverTagComp:boolean=false;

  id:string = "";
  licensePlateNumber:string = "";
  containerNumber:string = "";
  inspectionWorkFlow:string = "";
  arrivedSiteDate:string = "";
  Queue:string = "";
  destiontion:string = "";

  ngOnInit(): void {
    this.id = this.driverTag.id;
    this.licensePlateNumber = this.driverTag.licensePlateNumber;
    this.containerNumber = this.driverTag.containerNumber;
    this.inspectionWorkFlow = this.driverTag.inspectionWorkFlow;
    this.arrivedSiteDate = this.driverTag.arrivedSiteDate;
    this.Queue = '001';
    this.destiontion = 'חניה לפני שיקוף';
}

}
