import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SSEComponent } from './ssecomponent.component';

describe('SSEComponent', () => {
  let component: SSEComponent;
  let fixture: ComponentFixture<SSEComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SSEComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SSEComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
