import { Component, OnInit } from '@angular/core';
//import { SharedService } from '../shared.service';

function hello() {
    alert('Hello!!!');
}
@Component({
  selector: 'app-ssecomponent',
  templateUrl: './ssecomponent.component.html',
   styleUrls: ['./ssecomponent.component.scss']
})
export class SSEComponent implements OnInit {
    constructor() { }//private serv: SharedService) { }
     ngOnInit() {
        //this.serv.returnAsObservable().subscribe(data => console.log(data));
        alert('Page Is on');
    }
    source: EventSource;
    GetExchangeData() {
        alert('Get Start');
     //   this.serv.GetExchangeData2();
        alert('GetExchangeData2 2');
       // this.http.get<any>(this.APIUrl + '/User/GetUserDisplayColumns');
        this.source = new EventSource('https://localhost:5001/api/sse');

        this.source.onmessage = function (event) {
            console.log('onmessage: ' + event.data);
        };

        this.source.onopen = function (event) {
            console.log('onopen');
        };

        this.source.onerror = function (event) {
            console.log('onerror');
        }
        alert('Get End');
    }
    stopExchangeUpdates() {
        alert('Stop Start');
        this.source.close();
     //   this.serv.stopExchangeUpdates();
        alert('Stop End');
    }
}