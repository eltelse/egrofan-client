import { DialogLayoutDisplay, ConfirmBoxInitializer } from '@costlydeveloper/ngx-awesome-popup';

declare var $: any;

export default class NotificationUtils {

    private static title: string = 'Are you sure?';
    private static message: string = "Please confirm.";
    private static yesLabel: string = "Yes";
    private static noLabel: string = "No";

    constructor() { }

   static openConfirmBox(message: string, yesFn: any, noFn: any, title: string = "", yesLabel: string = "", noLabel: string = "") {
        // See documentation at: https://costlydeveloper.github.io/ngx-awesome-popup/#/
        const confirmBox = new ConfirmBoxInitializer();
        confirmBox.setTitle(title && title !== "" ? title : this.title);
        confirmBox.setMessage(message);
        confirmBox.setButtonLabels(yesLabel && yesLabel !== "" ? yesLabel : this.yesLabel, noLabel && noLabel !== "" ? noLabel : this.noLabel);

        // Choose layout color type
        confirmBox.setConfig({
            LayoutType: DialogLayoutDisplay.NONE, // SUCCESS | INFO | NONE | DANGER | WARNING
        });

        // Simply open the popup and listen which button is clicked
        confirmBox.openConfirmBox$().subscribe(resp => {
            // IConfirmBoxPublicResponse
            if (resp.Success === true)
                yesFn();
            else
                noFn();
        });
    };

    
  static showNotification(message: string, level: string = "info", title: string = "", v_align: string = "top", h_align: string = "left") {

    // level: '','info','success','warning','danger'
    // v_align: top/bottom
    // h_align: left/center,right

    $.notify({
      icon: "notifications",
      message: message,
      title: title,

    }, {
      type: level,
      timer: 2000,
      placement: {
        from: v_align,
        align: h_align
      },
      template: '<div data-notify="container" class="col-xl-4 col-lg-4 col-11 col-sm-4 col-md-4 alert alert-{0} alert-with-icon" role="alert">' +
        '<button mat-button  type="button" aria-hidden="true" class="close mat-button" data-notify="dismiss">  <i class="material-icons">close</i></button>' +
        '<i class="material-icons" data-notify="icon">notifications</i> ' +
        '<span data-notify="title">{1}</span> ' +
        '<span data-notify="message">{2}</span>' +
        '<div class="progress" data-notify="progressbar">' +
        '<div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
        '</div>' +
        '<a href="{3}" target="{4}" data-notify="url"></a>' +
        '</div>'
    });
  }

}