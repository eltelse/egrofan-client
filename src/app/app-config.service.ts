import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AppConfigService {

  private appConfig: any;

  constructor(private http: HttpClient) { }

  loadAppConfig() {
    return this.http.get('/assets/config/app-settings.json')
      .toPromise()
      .then(data => {
        this.appConfig = data;
      });
  }

  validateLoded(){
    if (!this.appConfig) {
      throw Error('Config file not loaded!');
    }
  }

  get paginatorPageSizeOptions() {
    this.validateLoded();
    return this.appConfig.paginatorPageSizeOptions;
  }

  get paginatorPageSize() {
    this.validateLoded();
    return this.appConfig.paginatorPageSize;
  }

  get sideBarTitle(){
    this.validateLoded();
    return this.appConfig.sideBarTitle;
  }

  get sseNotificationUrl(){
    this.validateLoded();
    return this.appConfig.sseNotificationUrl;
  }

  

}
