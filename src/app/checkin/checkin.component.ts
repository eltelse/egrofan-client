import { SharedService } from '../shared.service';
import { Component, Input, OnInit, Output, EventEmitter, Inject, ViewEncapsulation } from '@angular/core';



@Component({
  selector: 'app-checkin',
  templateUrl: './checkin.component.html',
  styleUrls: ['./checkin.component.scss']
})
export class CheckinComponent   implements OnInit {
    @Input() vecL: any;
    id: string = "";
    LicensePlateNumber: string = "";
    containerNumber: string = "";
    manifestNo: number = 2;
    remark: string = "";
    InspectionList: any;
    showPortal = false;
    driverTag: any;
    items: any[];
    vec: any;
    numPer: number = -1;
    imageSrcLPR = 'assets/img/lpr1.jpg';
    messageTextLPR = '';
    imageButtonsLPR = [{
        src: 'assets/img/lpr1.jpg', name: 'image-lpr1'
    }, { src: 'assets/img/lpr2.jpg', name: 'image-lpr2' },
    { src: 'assets/img/lpr3.jpg', name: 'image-lpr3' }]

    imageSrcCCR = 'assets/img/ccr1.jpg';

    messageTextCCR = '';

    imageButtonsCCR = [{
        src: 'assets/img/ccr1.jpg', name: 'image-ccr3'
    }]
    selectedInspectionType: any;

    constructor(private service: SharedService) { }

    setShow() {
        this.vecL.id = this.id;
        this.vecL.LicensePlateNumber = this.LicensePlateNumber;
        this.vecL.containerNumber = this.containerNumber;
        this.vecL.manifestNo = this.manifestNo;
        this.vecL.remark = this.remark;
        this.selectedInspectionType = { ipsName: this.vecL.inspectionWorkFlow }
        this.showPortal = true;
        this.containerNumber = "dd";
    }

    setNoShow() {
        this.showPortal = false;
    }
    

    loadInspectionList() {
        this.service.getInspectionTypesList().subscribe((dataIps: any) => {
            this.InspectionList = dataIps;
        })
    }

    compareFn(inspectTypeA: any, inspectTypeB: any) {
        return inspectTypeA && inspectTypeB && inspectTypeA.ipsName == inspectTypeB.ipsName;
    }



    @Output() messageEvent = new EventEmitter<string>();
    doBypass() {
        this.containerNumber = "dod";
        this.messageEvent.emit("");
    }


    receiveMessage(e: any) {
        this.messageEvent.emit("");
    }
    ngOnInit(): void {
      //  this.getVehicleList();
        this.id = this.vecL.id;
        this.LicensePlateNumber = this.vecL.LicensePlateNumber;
        this.containerNumber = this.vecL.containerNumber;
        this.numPer = (Math.floor(this.vecL.LicensePlateNumber / 1000)) % 100;
        this.loadInspectionList();

    }

    getVehicleList() {
        this.service.getVehicleList().subscribe(data => {

            this.items = data;
            this.vecL = data[2];
            this.id = this.vecL.id;
            this.LicensePlateNumber = this.vecL.LicensePlateNumber;
            this.containerNumber = this.vecL.containerNumber;
            this.numPer = (Math.floor(this.vecL.LicensePlateNumber / 1000)) % 100;

        })
    }

    ngOnChanges(): void {
        this.id = this.vecL.id;
        this.LicensePlateNumber = this.vecL.LicensePlateNumber;
        this.numPer = (Math.floor(this.vecL.LicensePlateNumber / 1000)) % 100;
        this.containerNumber = this.vecL.containerNumber;

        this.manifestNo = this.vecL.manifestNo;
        this.remark = this.vecL.remark;

        this.selectedInspectionType = { ipsName: this.vecL.inspectionWorkFlow }
    }

    onClickLPR(imageNameObject: any) {
        this.imageSrcLPR = imageNameObject.src;
        this.messageTextLPR = imageNameObject.name;
    }

    onClickCCR(imageNameObject: any) {
        this.imageSrcCCR = imageNameObject.src;
        this.messageTextCCR = imageNameObject.name;
    }

    src = 'assets/img/sdms-scan.pdf';
    scanReady: boolean = false;
    doScan(): void {
        this.scanReady = true;
    }
}





