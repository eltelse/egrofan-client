import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PrintDriverTagComponent } from './print-driver-tag.component';

describe('PrintDriverTagComponent', () => {
  let component: PrintDriverTagComponent;
  let fixture: ComponentFixture<PrintDriverTagComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PrintDriverTagComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PrintDriverTagComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
