import { Component, AfterContentInit , HostListener, Input, ViewChild, OnInit, ComponentFactoryResolver, ApplicationRef, Injector, OnDestroy } from '@angular/core';
import { CdkPortal, DomPortalHost } from '@angular/cdk/portal';


@Component({
  selector: 'app-print-driver-tag',
  templateUrl: './print-driver-tag.component.html',
  styleUrls: ['./print-driver-tag.component.scss']
})
export class PrintDriverTagComponent implements OnInit, OnDestroy  {
  @HostListener('window:beforeunload', ['$event'])
  onWindowClose(event: any): void {
    // Do something

    event.preventDefault();
    event.returnValue = false;

  }
  @Input() vecL: any;
  // STEP 1: get a reference to the portal

  // STEP 2: save a reference to the window so we can close it
  private externalWindow: any;

  private externalWindow2: any;

  // STEP 3: Inject all the required dependencies for a PortalHost
  constructor(
    private componentFactoryResolver: ComponentFactoryResolver,

    private injector: Injector) { }


  ngOnInit() {
    // STEP 4: create an external window


           //hr {width: 459px; float: right;border: 1px solid black;}\
       //body {   width: 660px;}\


    this.externalWindow = window.open('', 'driver-tag.component', 'width=560,height=400,left=200,top=200');
    this.externalWindow.document.write("<style>label { float:right; font-weight: bold;  font-size: 25px; display: inline-block; width: 187px;  text - align: right}\
      .col-2 { margin-right: 64px;}\
      hr {width: 459px; float: right;border: 1px solid black;}\
      img { float: right; font-weight: bold; font-size: 25px; display: inline-block; width: 255px; margin-right: 104px; height: 90px;}\
      </style>\
      <div>\
      <div>\
      <label class=\"col-1\" >:מספר בדיקה </label>\
    <label class=\"col-2\" >"+ this.vecL.id + " </label>\
      </div><hr>\
      <div>\
      <label class=\"col-1\" >:מספר רישוי </label>\
        <label class=\"col-2\" >"+ this.vecL.licensePlateNumber + " </label>\
       </div><hr>\
      <div>\
        <label class=\"col-1\" >:מספר מכולה</label>\
      <label class=\"col-2\" >"+ this.vecL.containerNumber + " </label>\
        </div><hr>\
            <div>\
      <label class=\"col-1\" >      :יעד</label>\
        <label class=\"col-2\" >חניה לפני שיקוף </label>\
        </div><hr>\
            <div>\
      <label class=\"col-1\" >:סוג בדיקה</label>\
        <label class=\"col-2\" >"+ this.vecL.inspectionWorkFlow + " </label>\
        </div><hr>\
            <div>\
      <label class=\"col-1\" >:מספר תור</label>\
        <label class=\"col-2\" >001 </label>\
        </div><hr>\
            <div>\
      <label class=\"col-1\" >:זמן כניסה</label>\
        <label class=\"col-2\" >"+ this.vecL.arrivedSiteDate + " </label>\
       <hr>\
      <div>\
        <img src=\"assets/img/barcode1.JPG\">\
      </div>\
      <div>\
      <label style=\"margin-right: 90px\"> "+ this.vecL.id + " </label>\
      </div><hr>\
        </div>\
      ");
    this.externalWindow.print();
    // STEP 5: create a PortalHost with the body of the new window document    
  // const host = new DomPortalHost(
   //   this.externalWindow.document.body,
   //   this.componentFactoryResolver,
   //   this.applicationRef,
   //   this.injector
   // );

    // STEP 6: Attach the portal
 //   host.attach(this.portal);
  }

  ngOnChanges(): void {

    //externalWindow.window.open('', 'driver-tag.component', 'width=560,height=400,left=200,top=200');
    this.externalWindow.document.body.innerHTML = '';
    this.externalWindow.document.write("<div>\
      <div>\
      <label class=\"col-1\" >מספר בדיקה </label>\
    <label class=\"col-2\" >"+ this.vecL.id + " </label>\
      </div><hr>\
      <div>\
      <label class=\"col-1\" >מספר רישוי </label>\
        <label class=\"col-2\" >"+ this.vecL.licensePlateNumber + " </label>\
       </div><hr>\
      <div>\
        <label class=\"col-1\" >מספר מכולה</label>\
      <label class=\"col-2\" >"+ this.vecL.containerNumber + " </label>\
        </div><hr>\
            <div>\
      <label class=\"col-1\" >יעד</label>\
        <label class=\"col-2\" >חניה לפני שיקוף </label>\
        </div><hr>\
            <div>\
      <label class=\"col-1\" >סוג בדיקה</label>\
        <label class=\"col-2\" >"+ this.vecL.inspectionWorkFlow + " </label>\
        </div><hr>\
            <div>\
      <label class=\"col-1\" >מספר תור</label>\
        <label class=\"col-2\" >001 </label>\
        </div><hr>\
            <div>\
      <label class=\"col-1\" >זמן כניסה</label>\
        <label class=\"col-2\" >"+ this.vecL.arrivedSiteDate + " </label>\
        </div><hr>\
        </div>\
      ");
   // this.externalWindow.print();
  }

  setCloseIt() {
    this.externalWindow.document.write("<style>label { float:right; font-weight: bold;  font-size: 25px; display: inline-block; width: 267px;  text - align: right}\
      .col-1 { margin-right: -142px;}\
      .col-2 { margin-right: 64px;}\
       hr {width: 459px; float: right;border: 1px solid black;\
       body {   width: 524px;}\
      </style>\
      <label class=\"col-1\" >מספר בדיקה </label>");
  //  this.externalWindow.open(location, '_self').close();
    // STEP 7: close the window when this component destroyed
  //  this.externalWindow.close()
  }

  ngOnDestroy() {
    this.externalWindow.open(location, '_self').close();
    // STEP 7: close the window when this component destroyed
    this.externalWindow.close()
  }
}

