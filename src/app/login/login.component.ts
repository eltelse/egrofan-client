import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, NgForm, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import NotificationUtils from 'app/utils/notification-utils';
import { SharedService } from '../shared.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit {

  constructor(private service: SharedService, private router: Router, private fb: FormBuilder) { }
  hidePassword: boolean = true;
  logedInUser: any = [];
  form: FormGroup;

  Email: string = "";
  Password: string = "";

  ngOnInit(): void {
    this.form = this.fb.group({
      Email: ['', [Validators.required, Validators.email]],
      Password: ['', Validators.required],
    });

  }

  // See: https://angular.io/api/forms/Validators
  // See: http://www.techtutorhub.com/article/Angular-Material-Form-Group-Validation-Dynamic-Error-Messages/68
  public checkInputError = (controlName: string, errorName: string) => {
    return this.form.controls[controlName].hasError(errorName);
  }

  validateUser() {
    this.service.validateUserCredentials(this.form.value).subscribe(
      res => {
        if (res.status === 'Success') {
          this.logedInUser = res.obj;
          this.router.navigateByUrl('/map');
        }
        else {
          NotificationUtils.showNotification(res.status, 'danger', 'Login failed', 'top', 'center');
        }
      });
  }
}
