import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { UtilsModule } from 'app/utils/utils.module';

import { CheckinFlowComponent } from './checkin-flow/checkin-flow.component';


@NgModule({
  declarations: [
    CheckinFlowComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    BrowserModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule,
    NgbModule,
    BrowserAnimationsModule,

    

    UtilsModule, // Angulare Material + More

  ],
  exports: [
    CheckinFlowComponent
  ]
})
export class WorkflowsModule { }
