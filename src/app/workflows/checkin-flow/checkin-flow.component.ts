import { Component, OnInit, Inject } from '@angular/core';
import { SharedService } from 'app/shared.service';
import PerfectScrollbar from 'perfect-scrollbar';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import NotificationUtils from 'app/utils/notification-utils';
import { DomSanitizer } from '@angular/platform-browser';
import { validateAdditionalItems } from 'ajv/dist/vocabularies/applicator/additionalItems';

@Component({
  selector: 'app-checkin-flow',
  templateUrl: './checkin-flow.component.html',
  styleUrls: ['./checkin-flow.component.scss']
})
export class CheckinFlowComponent implements OnInit {

  constructor(private service: SharedService, private fb: FormBuilder, public dialogRef: MatDialogRef<CheckinFlowComponent>, @Inject(MAT_DIALOG_DATA) public data: any, private sanitizer: DomSanitizer) {

    this.id = this.data.info.id;
    this.Identifier = this.data.info.Identifier
    this.WorkflowTypeId = this.data.info.WorkflowTypeId;
    this.LPR = this.data.info.LPR;
    this.CCR = this.data.info.CCR;
    this.ManifestCode = this.data.info.ManifestCode;
    this.Remark = this.data.info.Remark;
  }

  save_success = "Inspection has been successfully started.";
  save_failiur = "Error while starting inspection. Message: ";
  bypass_success = "Asking for bypass - success.";
  bypass_failiur = "Asking for bypass error. Message: ";

  form: FormGroup;

  tabIndex = 0;

  id: number = -1;
  Identifier: string = "";
  WorkflowTypeId: number = -1;
  LPR: string = "";
  LPRCorrectionTypeId: number = 0;
  CCR: string = "";
  CCRCorrectionTypeId: number = 0;
  ManifestCode: string = "";
  Remark: string = "";
  enableSubmit: boolean = false;

  InspectionTypesList: any = [];

  ngOnInit(): void {
    this.loadInspectionTypesList();

    this.form = this.fb.group({
      LPR: ['', Validators.required],
      CCR: ['', Validators.required],
      ManifestCode: ['', Validators.required],
      Remark: [''],
      WorkflowTypeId: [-1],

    });


  }


  loadInspectionTypesList() {
    this.service.getInspectionTypesList().subscribe((types: any) => {
      this.InspectionTypesList = types;
    })
  }

  ngOnChanges(): void {

  }

  ngAfterViewInit(): void {
    const elemMainPanel = <HTMLElement>document.querySelector('.scrallbarWrapper');
    const ps = new PerfectScrollbar(elemMainPanel);
    ps.update();
  }

  startInspection() {

    this.vlidateCorrections();

    if (this.save()) {

      console.log('TBD - Update manifest and images')
      console.log('TBD - Print driver tag')

      this.dialogRef.close(this.form.value);
    }



  }

  vlidateCorrections() {

    if (this.LPR != this.data.info.LPR) {
      this.LPRCorrectionTypeId = 2; // MANUAL
    }

    if (this.CCR != this.data.info.CCR) {
      this.CCRCorrectionTypeId = 2; // MANUAL
    }

  }

  save(): boolean {
    //this.dialogRef.close(this.form.value);
    let res: boolean = true;
    this.service.updateInspection(this.getEditObjForSave()).subscribe(res => {
      if (res && res === 'Success') {
        NotificationUtils.showNotification(this.save_success, 'success');
        res = true;
      }
      else {
        NotificationUtils.showNotification(this.save_failiur + res.toString(), 'danger');
        res = false;
      }
    });

    return res;
  }

  public checkInputError = (controlName: string, errorName: string) => {
    return this.form.controls[controlName].hasError(errorName);
  }

  getEditObjForSave() {
    return {
      id: this.id,
      Identifier: this.Identifier,
      WorkflowTypeId: this.WorkflowTypeId,
      LPR: this.LPR,
      LPRCorrectionTypeId: this.LPRCorrectionTypeId,
      CCR: this.CCR,
      CCRCorrectionTypeId: this.CCRCorrectionTypeId,
      ManifestCode: this.ManifestCode,
      DriverTagCode: this.Identifier,
      Remark: this.Remark,
    }
  }

  scan() {
    console.log('Scanning...');
  }

  bypass() {
    this.service.askForBypass(this.id.toString()).subscribe(res => {
      if (res && res === 'Success') {
        //NotificationUtils.showNotification(this.bypass_success, 'success');
        res = true;
      }
      else {
        NotificationUtils.showNotification(this.bypass_failiur + res.toString(), 'danger');
        res = false;
      }
    });

  }

  dismiss(): void {
    this.dialogRef.close();
  }

  addLPRImage() {
    console.log('add actions!!');
  }

  removeLPRImage(index: number) {
    let that = this;
    NotificationUtils.openConfirmBox("Are you sure?",
      function () {
        that.data.images.LPR.splice(index, 1);
      },
      function () {

      }, 'Confirm Image Removal', 'Remove', 'Cancel');

    console.log('removal actions!!');
  }

  addCCRImage() {
    console.log('add actions!!');
  }

  removeCCRImage(index: number) {
    let that = this;
    NotificationUtils.openConfirmBox("Are you sure?",
      function () {
        that.data.images.CCR.splice(index, 1);
      },
      function () {

      }, 'Confirm Image Removal', 'Remove', 'Cancel');

    console.log('removal actions!!');
  }

  scanDocument() {
    this.tabIndex = 1;
    console.log('add scan actions!!');
  }

  removeDocument(index: number) {
    let that = this;
    NotificationUtils.openConfirmBox("Are you sure?",
      function () {

        that.data.images.Manifest.splice(index, 1);
      },
      function () {

      }, 'Confirm Document Removal', 'Remove', 'Cancel');

    console.log('removal actions!!');
  }

  getSafeSrc(unsafe: string) {
    return this.sanitizer.bypassSecurityTrustResourceUrl(unsafe)
  }

}
