import { TestBed } from '@angular/core/testing';

import { ServerSideNotifyService } from './sse-notify.service';

describe('CheckinNotifyService', () => {
  let service: ServerSideNotifyService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ServerSideNotifyService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
