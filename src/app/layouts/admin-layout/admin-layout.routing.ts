import { Routes } from '@angular/router';

import { LoginComponent } from '../../login/login.component';
import { MapComponent } from '../../map/map.component';

import { UserListComponent } from '../../management/user-list/user-list.component';
import { VehicleComponent } from '../../management/vehicle/vehicle.component';
import { WorkStationComponent } from 'app/management/work-station/work-station.component';
import { InspectionComponent } from 'app/management/inspection/inspection.component';
import { GenericTypeCrudComponent } from 'app/management/generic-type-crud/generic-type-crud.component';


import { DriverTagComponent } from '../../driver-tag/driver-tag.component';
import { NotificatoinComponent } from 'app/management/notificatoin/notificatoin.component';



export const AdminLayoutRoutes: Routes = [
    // Root level 
    { path: 'login', component: LoginComponent },
    { path: 'map', component: MapComponent },
    { path: 'driver-tag', component: DriverTagComponent },
    
    // Management
    { path: 'work-station', component: WorkStationComponent },
    { path: 'user-list', component: UserListComponent },
    { path: 'inspection', component: InspectionComponent },
    { path: 'notification', component: NotificatoinComponent },
    { path: 'vehicle', component: VehicleComponent },
    { path: 'AreaType', component: GenericTypeCrudComponent },
    { path: 'ConclusionType', component: GenericTypeCrudComponent },
    { path: 'DataCorrectionType', component: GenericTypeCrudComponent },
    { path: 'DeviceCommandSourceType', component: GenericTypeCrudComponent },
    { path: 'DeviceEventType', component: GenericTypeCrudComponent },
    { path: 'DeviceEventType', component: GenericTypeCrudComponent },
    { path: 'DeviceStatusType', component: GenericTypeCrudComponent },
    { path: 'DeviceType', component: GenericTypeCrudComponent },
    { path: 'InspectionDataType', component: GenericTypeCrudComponent },
    { path: 'InspectionHistorySourceType', component: GenericTypeCrudComponent },
    { path: 'InspectionWorkflowType', component: GenericTypeCrudComponent },
    { path: 'VehicleType', component: GenericTypeCrudComponent },
    { path: 'WorkStationType', component: GenericTypeCrudComponent },
    { path: 'NotificationType', component: GenericTypeCrudComponent },








];
