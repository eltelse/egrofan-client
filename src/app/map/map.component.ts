import { Component, OnInit } from '@angular/core';
import { ServerSideNotifyService } from 'app/sse-notify.service';

import { MatDialog, MatDialogConfig, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Overlay } from '@angular/cdk/overlay';
import { CheckinFlowComponent } from 'app/workflows/checkin-flow/checkin-flow.component';


@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.css']
})
export class MapComponent implements OnInit {

  constructor(private sseNotificationsServ: ServerSideNotifyService, public dialog: MatDialog, private overlay: Overlay) { }


  checkinData: any = {
    info: {
      id: 1,
      Identifier: "202201110064",
      WorkflowTypeId: 2,
      LPR: "1122335544",
      LPRQualifier: 80,
      CCR: "ASDF123456",
      CCRQualifier: 70,
      ManifestCode: 'ABC1234GH',
      Remark: 'Test',
    },
    images: {
      LPR: [
        { name: '145034_CarLeftUpper.jpg', device: 'BIA1OCR1', src: 'assets/img/145034_CarLeftUpper.jpg', value: '1234567', qualifier: '80' }
      ],
      CCR: [
        { name: '145034_ContLeftUpper_f', device: 'BIA1OCR1', src: 'assets/img//145034_ContLeftUpper_f.jpg', value: '------', qualifier: '0' },
        { name: '145034_ContTop_f', device: 'BIA1OCR1', src: 'assets/img/145034_ContTop_f.jpg', value: 'MCLU123456 7', qualifier: '70' }
      ],
      IAW: [],
      Manifest: [
        { name: 'sdms-scan.pdf', device: '', src: 'assets/img/sdms-scan.pdf', value: '', qualifier: '' },
        { name: 'sdms2', device: '', src: 'assets/img/sdms-scan.pdf', value: '', qualifier: '' },
      ]
    }
  }

  ngOnInit(): void {

    this.sseNotificationsServ.notificationsListenStart();

    this.sseNotificationsServ.getCheckinAsObservable().subscribe(
      next => this.handleServerSideNotifications(next),
      error => this.handleServerSideNotificationErrors(error)
    );


  }

  handleServerSideNotifications(data: any) {
    console.log('in MAP handleServerSideNotifications. Data: ');
    console.log(data);
    // TODO: pop up check in
  }

  handleServerSideNotificationErrors(error: any) {
    console.log('in MAP handleServerSideNotificationErrors. Error: ' + error);
  }

  openDialog(): void {
    const dialogConfig = new MatDialogConfig();
    const scrollStrategy = this.overlay.scrollStrategies.reposition();
    dialogConfig.data = this.checkinData;
    dialogConfig.scrollStrategy = scrollStrategy;
    dialogConfig.width = "100%";
    //dialogConfig.height = "90%";
    const dialogRef = this.dialog.open(CheckinFlowComponent, dialogConfig);

    dialogRef.afterClosed().subscribe((data) => {
      //if (data)
      //this.checkinData = data;
      //console.log('this.checkinData - ', this.checkinData);
    });
  }

}


